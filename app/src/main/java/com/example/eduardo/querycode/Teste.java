package com.example.eduardo.querycode;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Teste extends AppCompatActivity {
    public Conexao conectar = new Conexao();

    Button conect;
    EditText ip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teste);

        conect = (Button) findViewById(R.id.conect);
        ip = (EditText) findViewById(R.id.ip);
    }


    public void getIp(View view){

        conectar.setIP(ip.getText().toString());
        Intent Login = new Intent(this, QueryCode.class);
        startActivity(Login);
    }
}
