package com.example.eduardo.querycode;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Conta extends AppCompatActivity {
    Metodos m = new Metodos();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conta);

        ListView ListPed = (ListView) findViewById(R.id.ListPed);
        ArrayList<String> pedidos = m.getDados();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, pedidos);
        ListPed.setAdapter(arrayAdapter);

        TextView totalp = (TextView) findViewById(R.id.totalp);
        //if("12345670".equals(m.Retorno())) {
            totalp.setText("Total a pagar: R$"+m.Pagar()+",00");

        //}
    }


    public void goPagamento(View view)
    {
        Intent pagamento = new Intent(this, FormaPagamento.class);
        startActivity(pagamento);
    }
}
