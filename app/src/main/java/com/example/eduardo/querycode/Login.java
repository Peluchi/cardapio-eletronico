package com.example.eduardo.querycode;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    Button button, button2;
    EditText Login, Password;

    public void startSecondActivity(View view) {

        Intent QueryCode = new Intent(this, QueryCode.class);
        startActivity(QueryCode);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Login = (EditText) findViewById(R.id.Login);
        Password = (EditText) findViewById(R.id.Password);
        button = (Button)findViewById(R.id.button);

      //  ImageView imagem2 = (ImageView) findViewById(R.id.imagem2);
       // imagem2.setImageResource(R.drawable.cadeado);



    }

    public void Entrar (View view){
        if(Login.getText().toString().equals("admin") && Password.getText().toString().equals("admin")){
            Toast.makeText(getApplicationContext(), "Redirecionando...", Toast.LENGTH_SHORT).show();
            Intent QueryCode = new Intent(this, QueryCode.class);
            startActivity(QueryCode);
        } else {
            Toast.makeText(getApplicationContext(), "Usuário ou senha inválida", Toast.LENGTH_SHORT).show();
        }

    }

    public void Cancelar (View view)
    {
        finish();
    }

}
