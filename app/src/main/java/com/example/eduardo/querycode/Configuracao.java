package com.example.eduardo.querycode;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Configuracao extends AppCompatActivity {
    public Conexao conectar = new Conexao();
    public Metodos rest = new Metodos();
    AlertDialog alertDialog;
    Button conect;
    EditText ip,restaurante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracao);

        conect = (Button) findViewById(R.id.conect);
        ip = (EditText) findViewById(R.id.ip);
        restaurante = (EditText) findViewById(R.id.restaurante);
        alertDialog = new AlertDialog.Builder(this).create();
    }

    public void getIp(View view){

        if("".equals(restaurante.getText().toString()) || "".equals(ip.getText().toString()))
        {
            alertDialog.setTitle("Erro");
            alertDialog.setMessage("Por favor preencha todos os campos.");
            alertDialog.show();
        }else {
            conectar.setIP(ip.getText().toString());
            rest.setRestaurante(restaurante.getText().toString());


            alertDialog.setTitle("Configurações");
            alertDialog.setMessage("Configurações salvas");
            alertDialog.show();
        }
    }
}
