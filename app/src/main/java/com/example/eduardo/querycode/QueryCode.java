package com.example.eduardo.querycode;

import android.app.DownloadManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import static com.example.eduardo.querycode.Metodos.alertDialog;

public class QueryCode extends AppCompatActivity {
    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    private TextView formatTxt, contentTxt;
    int teste = 0;
    AlertDialog alertDialog, alertDialog2;
    Metodos m = new Metodos();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query_code);
        formatTxt = (TextView)findViewById(R.id.scan_format);
        contentTxt = (TextView)findViewById(R.id.scan_content);
        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog2 = new AlertDialog.Builder(this).create();

        alertDialog2.setTitle("Boas Vindas:");
        //alertDialog2.setMessage("bem vindo ao "+m.getRestaurante()+", bom apetite.");
        alertDialog2.setMessage("Bem vindo, bom apetite.");
        alertDialog2.show();

    }

    public void scanBar(View v) {
        try {

            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {

            showDialog(QueryCode.this, "Scanner não encontrado", "Faça Download de um scanner activity?", "Sim", "Não").show();
        }
    }

    public void scanQR(View v) {
        try {

            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {

            showDialog(QueryCode.this, "Scanner não encontrado", "Faça Download de um scanner activity?", "Sim", "Não").show();
        }
    }

    public void testePedido(View view)
    {
        Intent Pedido = new Intent(this, Pedido.class);
        startActivity(Pedido);
    }


    private static AlertDialog showDialog(final AppCompatActivity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {

                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return downloadDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {


                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                m.setReturn(contents);
                formatTxt.setText("FORMAT: " + format);
                contentTxt.setText("CONTENT: " + m.Retorno());


                Toast toast = Toast.makeText(this, "Leitura:" + m.Retorno() + " Formato:" + format, Toast.LENGTH_LONG);
                toast.show();
                if("QR_CODE".equals(format)) {
                    Intent Pedido = new Intent(this, Pedido.class);
                    startActivity(Pedido);
                }else {
                   // Intent Conta = new Intent(this, Conta.class);
                    //startActivity(Conta);
                    alertDialog.setTitle("Registro:");
                    alertDialog.setMessage("Cardápio associado a mesa 01");
                    alertDialog.show();
                }
            }
        }
    }

    public void Conta(View view)
    {
        Intent i = new Intent(this, Conta.class);
        startActivity(i);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void config(View view)
    {
        Intent i = new Intent(this, Configuracao.class);
        startActivity(i);
    }
}
