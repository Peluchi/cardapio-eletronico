package com.example.eduardo.querycode;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import static com.example.eduardo.querycode.Metodos.alertDialog;

public class FormaPagamento extends AppCompatActivity {

    RadioGroup group;
    Conexao conect = new Conexao();
    Metodos met = new Metodos();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forma_pagamento);

        alertDialog = new AlertDialog.Builder(this).create();
        group = (RadioGroup) findViewById(R.id.group);

        switch (group.getCheckedRadioButtonId()) {
            case R.id.dinheiro:

                met.setTipoPag("dinheiro");
                break;
            case R.id.credito:

                met.setTipoPag("credito");
                break;
        }
    }


    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (group.getCheckedRadioButtonId()) {
            case R.id.dinheiro:
                if (checked)
                    met.setTipoPag("dinheiro");
                break;
            case R.id.credito:
                if (checked)
                    met.setTipoPag("credito");
                break;
        }
    }

    public void PedirConta(View view)
    {
        String pedido = met.getTipoPag();

        alertDialog.setTitle("Conta:");
        alertDialog.setMessage("Obrigado pela preferência, a conta ja será entregue.");
        alertDialog.show();
        conect.EnviarTipoPagamento(pedido);
    }



    }

