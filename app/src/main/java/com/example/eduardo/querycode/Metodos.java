package com.example.eduardo.querycode;

import android.app.AlertDialog;

import java.util.ArrayList;

/**
 * Created by Eduardo on 04/11/2016.
 */
public class Metodos {
    public static String retorno, tipo, restaurante;
    public static int conta;
    public static int id = 1;
    static AlertDialog alertDialog;
    public static ArrayList<String> dados = new ArrayList<String>();
    public static void setReturn(String i)
    {    retorno = i;
    }

    public static String Retorno()
    {
        return retorno;
    }

    public static void setConta(int c)
    { int number = c;



            conta = conta + number;

    }

    public static ArrayList<String> addDados(String prato, String valor, String qtd, String pagar) {


        dados.add("Pedido "+id+": "+prato+"\n Valor: "+valor+"\n Quantidade: "+qtd+"\n Valor Pedido: "+pagar);
        id++;
        return dados;

    }
    public static ArrayList<String> getDados() {


        return dados;
    }

    public static void setTipoPag(String pag)
    {
         tipo = pag;
    }

   public static String getTipoPag()
   {
       return tipo;
   }



    public static String Pagar()
    {
        return String.valueOf(conta);
    }

    public void setRestaurante(String rest)
    {
        restaurante = rest;
    }

    public String getRestaurante()
    {
        return restaurante;
    }
}
