package com.example.eduardo.querycode;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Pedido extends AppCompatActivity {
    Metodos m = new Metodos();
    Conexao conect = new Conexao();
    String teste;
    EditText qtd;
    int precoC = 50, precoM = 25, precoP = 15;
    public TextView total,desc;
    AlertDialog alertDialog;
    int total1;
    TextView valor, prato;
    Button button3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);

        alertDialog = new AlertDialog.Builder(this).create();


        ImageView imagem = (ImageView) findViewById(R.id.imagem);
        prato = (TextView) findViewById(R.id.prato);
        valor = (TextView) findViewById(R.id.valor);
         total = (TextView) findViewById(R.id.total);
         qtd = (EditText) findViewById(R.id.qtd);
         desc = (TextView) findViewById(R.id.desc);
        button3 = (Button)findViewById(R.id.button3);



        prato.setText(m.Retorno());
        teste = m.Retorno();
        if("bife".equals(teste))
        {
            valor.setText("R$"+precoC+",00");

            imagem.setImageResource(R.drawable.bife);
            desc.setText("Filé,tomate,pimentão,cebola.");
        }else if("macarrao".equals(teste))
        {
            valor.setText("R$"+precoM+",00");

            imagem.setImageResource(R.drawable.maca);
            desc.setText("Macarrão com verduras.");
        }else if("panqueca".equals(teste))
        {
            valor.setText("R$"+precoP+",00");
            imagem.setImageResource(R.drawable.panq);
            desc.setText("Panqueca bolonhesa.");
        }
    }


    public void cacular(View view)
    {
        if("".equals(qtd.getText().toString()))
        {
            alertDialog.setTitle("Quantidade:");
            alertDialog.setMessage("Insira a quantidade para calcular");
            alertDialog.show();
        }else {
            int qt1 = Integer.parseInt(qtd.getText().toString());


            if ("bife".equals(m.Retorno())) {
                total1 = (qt1 * precoC);
                total.setText("R$" + String.valueOf(total1) + ",00");

            } else if ("macarrao".equals(m.Retorno())) {
                total1 = (qt1 * precoM);
                total.setText("R$" + String.valueOf(total1) + ",00");

            } else if ("panqueca".equals(m.Retorno())) {
                total1 = (qt1 * precoP);
                total.setText("R$" + String.valueOf(total1) + ",00");

            }
        }

    }



    public void efetuarP(View view)
    {

        if(("".equals(qtd.getText().toString()))&& "".equals(total.getText().toString())) {

            alertDialog.setTitle("Pedido:");
            alertDialog.setMessage("Escolha a quantidade e calcule o valor, obrigado.");
            alertDialog.show();



    }else
    {
        m.setConta(total1);
        String nomep = prato.getText().toString();
        String valorp = valor.getText().toString();
        String quantidade = qtd.getText().toString();
        String pagar = total.getText().toString();

        m.addDados(nomep,valorp,quantidade,pagar);
        alertDialog.setTitle("Pedido:");
        alertDialog.setMessage("Seu pedido foi realizado com sucesso, bom jantar.");
        alertDialog.show();
        conect.Enviar(nomep,quantidade);

    }
    }
}
