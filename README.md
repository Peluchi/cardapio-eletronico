# README #

# Cardápio Eletrônico #

O cardápio eletrônico é uma solução prática para bares e restaurantes que utilizando o sistema de comandas para pedido, informatizando o cardápio, os pedidos e a conta. Esta solução visa agilizar o atendimento e satisfazer clientes e estabelecimentos, trazendo consigo maior velocidade no atendimento e evitando problemas de pedidos errados e longas filas para pagamento e até mesmo a dificuldade de encontrar um garçom disponível.

Para mais informações técnicas acesse a [wiki do projeto.](https://bitbucket.org/Peluchi/cardapio-eletronico/wiki/Home)

## Releases ##

* Version 0.1 beta (em desenvolvimento)

## Features ##

* ###Uma aplicação simples e segura para controle de clientes, mesas e pedidos.###
* ###Cadastro de cardápios dentro de uma rede local ou web.###

   1. O cliente poderá escolher entre utilizar a aplicação diretamente nas mesas, com dispositivos fixados ou o próprio cliente pode utilizar seu smartphone para acessar o serviço por meio de um aplicativo (ainda não disponível)

* ###Sistema de calculo de comandas automático###
* ###Mesas vinculadas à comandas via QRCode ou BarCode###
* ###Adição ou remoção de ingredientes nos pedidos de forma simples e intuitiva###
* ###Botão para solicitar um garçom###

## Diretivas de Contribuições ##

* ###Escrita de testes###

   1. Os testes devem ser simples, assim como o código para evitar problemas desnecessário com a complexidade dos testes.

   2. Os testes devem ser padronizados e escritos em classes específicas

   3. É importante que os nomes dos métodos de testes sejam sugestivos e de fácil compreensão

* ###Revisão de código###

   1. Todos os códigos podem ser revisados

   2. Obrigatoriamente códigos revisados devem seguir o padrão de nomeação de variáveis, mantendo a nomeação simples, porém evitando variáveis que não sugestem seu uso, como x,y,x1 etc...

   3. Bugs e erros encontrados durante a revisão devem ser reportados via Issue, informando o nome do arquivo, a linha ou linhas das ocorrências e uma rápida descrição dos erros. Quando necessário pode-se copiar o log dos erros.

   4. Erros de sintaxe podem ser corrigidos nesta etapa e commitados.

* ###Solicitação de incrementos, módulos ou melhorias###

   1. Ticktes de sugestões devem ser criados na sessão de [Issues](https://bitbucket.org/Peluchi/cardapio-eletronico/issues?status=new&status=open) deste projeto

   2. Solicitações de incrementos, módulos e/ou melhorias serão analisadas e não obrigatoriamente serão implementas, ou podem ser adiadas para versões futuras.

   3. Este tipo de solicitação deve ser determinado como Proposal ou Enhancement.

* ###Reportar bugs###

   1. Bugs devem ser reportados na sessão [Issues](https://bitbucket.org/Peluchi/cardapio-eletronico/issues?status=new&status=open) deste projeto.

   2. Todo Bug deve ser reportado juntamente com uma rápida descrição do cenário em que ele ocorreu e com o erro apresentado, este erro pode ser apresentado no corpo do texto ou anexando uma screenshot do erro.

   3. Antes de reportar um Bug verificar se este é relacionado à solução propriamente dita ou decorrente de aplicações externas.


## Com quem falar? ##

* [Eduardo Peluchi](https://www.facebook.com/duuhhp)

* [Luis Gustavo Corrêa](https://www.facebook.com/luis.g.correa.161)